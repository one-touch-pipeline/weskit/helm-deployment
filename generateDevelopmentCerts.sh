#!/bin/bash

# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

# This script generates self signed development certificates.
# Per default the certificates will be stored in the certs sub-folder.
# The path can be changed by setting command line arg 1
# It will not overwrite existing certificates

# localhost and hostname of the generating machine are valid dns names for the certificate!

certStoragePath="${1:?No storage path provided}"
hostName="${2:-}"
hostIp="${3:-}"
mkdir -p "$certStoragePath"

# The certificate has to contain the name and/or IP of the service in the CN and subjectAltName
# records.

prefix="$certStoragePath/weskit"
if [ ! -f $prefix".key.pem" ] || [ ! -f $prefix".crt.pem" ]; then
    echo "Least one certificate file missing / not found!"  > /dev/stderr
    echo "Generating new certificate and write it to: "$certStoragePath".*"  > /dev/stderr

    # Generate Self Signed Cert
    openssl req \
      -x509 \
      -newkey rsa:4096 \
      -sha256 \
      -days 365 \
      -nodes \
      -keyout $prefix.key.pem \
      -out $prefix.crt.pem \
      -subj "/CN=${hostName:+localhost}" \
      -addext "subjectAltName = DNS:localhost,IP:127.0.0.1${hostName:+,DNS:$hostName}${hostIp:+,IP:$hostIp}"

    # Convert Certificate to PEM Format needed for requests from python
    openssl x509 \
      -in $prefix.crt.pem \
      -text \
      -out $prefix.crt

    openssl rsa \
      -in $prefix.key.pem \
      -text \
      -out $prefix.key
else
  echo "Using existing certificate!" > /dev/stderr
fi

chmod 0750 certs/
chmod -R 0640 certs/*


