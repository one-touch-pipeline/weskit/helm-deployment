# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

[uwsgi]
# https://uwsgi-docs.readthedocs.io/en/latest/Options.html
# https://www.bloomberg.com/company/stories/configuring-uwsgi-production-deployment/

# Request Logging
{{- if .Values.debugServices }}
log-4xx = true
log-5xx = true
{{- else }}
disable-logging = true
{{- end }}

# Safety
need-app = true    ; Starting the server without an app does not make sense
strict = true      ; All configuration options must exist (guard against typos)

shared-socket = 0.0.0.0:5000

# Parallelism configuration
master = true
auto-procname = true                 ; label processes
workers = 3

# To avoid impact of memory leaks
max-requests = 1000                  ; Restart workers after this many requests
max-worker-lifetime = 3600           ; Restart workers after this many seconds
reload-on-rss = 2048                 ; Restart workers after this much resident memory
worker-reload-mercy = 60             ; How long to wait before forcefully killing workers

# We have a single-threaded application
enable-threads = false
threads = 1

# Wot needed
single-interpreter = true

# Clean up sockets
vacuum = true

module = uwsgi_server.weskit_uwsgi:app
{{- if default true ((.Values.weskit).ssl).enabled }}
https = =0,/home/weskit/certs/weskit.crt.pem,/home/weskit/certs/weskit.key.pem,HIGH
{{- else }}
http = =0
{{- end }}

# Others, maybe:
# post-buffering = 1
