# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

# Enable this during running of tests
DEBUG: {{ .Values.debugServices }}

# development | production
{{- if ne .Values.volumes.code.storageClass "none" }}
ENV: &environment development
{{- else }}
ENV: &environment production
{{- end }}

# Enable this during running of tests.
TESTING: true

# Use a custom workdir for each run. This needs to be defined by tags field in request
require_workdir_tag: {{ default false .Values.weskit.require_workdir_tag }}

# 'executor' defines where the workflow engine is executed. Allowed values are
# "ssh", "ssh_lsf", "ssh_slurm", "local", "local_lsf", and "local_slurm"
#
# Remote executors (all but "local") need a remote_data_dir. This is the corresponding value for
# WESKIT_DATA, but located on the remote filesystem. "remote_data_dir" is ignored if the executor
# type is set to "local". If any of the executors involving SSH is used, additionally "login" is
# is needed, but is ignored for all "local" executor types.
executor:
{{- if not .Values.weskit.executor }}
  type: "local"
{{- else }}
  type: {{ .Values.weskit.executor.type | quote }}
  {{- if include "weskit.useSsh" . }}
  remote_data_dir: {{ include "weskit.executor.dataDir" . }}
  remote_workflows_dir: {{ include "weskit.executor.workflowsDir" . }}
  singularity_containers_dir: {{ include "weskit.executor.singularityContainerDir" . }}
  login:
    host: {{ required "SSH submission host is required" .Values.weskit.executor.remote.host | quote }}
    port: {{ default 22 .Values.weskit.executor.remote.port }}
    username: {{ required "Username is required" .Values.weskit.executor.remote.username | quote }}
    keyfile_passphrase: {{ .Values.weskit.executor.remote.passphrase | quote }}
{{- /* Instead the following should work, but the `lookup` always returns an empty map[] :( */}}
{{- /*    keyfile_passphrase: {{- (lookup "v1" "Secret" .Release.Namespace (default "weskit-ssh" (.Values.weskit.executor.login).existingSecret)).passphrase | b64dec | quote }}*/}}
    {{- /* The following are the paths in the REST and worker containers */}}
    knownhosts_file: {{ .Values.weskit.executor.remote.knownhosts_file  }}
    keyfile: {{ .Values.weskit.executor.remote.keyfile | quote }}
    keepalive_interval: {{ default "30s" (.Values.weskit.executor.remote).keepalive_interval | quote }}
    keepalive_count_max: {{ default 5 (.Values.weskit.executor.remote).keepalive_count_max }}
  {{- /*    {{- if .Values.weskit.executor.remote.retry_options }}*/}}
{{- /*    retry_options:*/}}
{{- /*    {{ .Values.weskit.executor.remote.retry_options | indent 6 }}*/}}
{{- /*    {{- else }}*/}}
{{- /*       wait_exponential:*/}}
{{- /*         multiplier: 1*/}}
{{- /*         min: 4*/}}
{{- /*         max: 300*/}}
{{- /*       wait_random:*/}}
{{- /*         min: 0*/}}
{{- /*         max: 1*/}}
{{- /*       stop_after_attempt: 5*/}}
{{- /*    {{- end }}*/}}
  {{- end }}
{{- end }}

static_service_info:

  # Commented out fields irrelevant for the ServiceInfo according to the Swagger file 1.0.0.
  #
  #    # Unique ID of this service. Reverse domain name notation is recommended, though not required.
  #    # The identifier should attempt to be globally unique, so it can be used in downstream
  #    # aggregator services e.g. Service Registry.
  #    id: "weskit.api"
  #
  #    # Name of this service. Should be human-readable.
  #    name: "WESkit"
  #
  #    # Type of GA4GH service
  #    type:
  #      # Namespace in reverse domain name format. Use org.ga4gh for implementations compliant with
  #      # official GA4GH specifications. For services with custom APIs not standardized by GA4GH, or
  #      # implementations diverging from official GA4GH specifications, use a different namespace
  #      # (e.g. your organization's reverse domain name).
  #      group: "weskit.api"
  #      # Name of the API or GA4GH specification implemented. Official GA4GH types should be
  #      # assigned as part of standards approval process. Custom artifacts are supported.
  #      artifact: "registry.gitlab.com/one-touch-pipeline/weskit/api"
  #      # Version of the API or specification. GA4GH specifications use semantic versioning.
  #      version: 1.0.0
  #
  #    # Description of the service. Should be human-readable and provide information about the
  #    # service.
  #    description: "WESkit - A GA4GH Compliant Workflow Execution Server"
  #
  #    organization:
  #      # Name of the organization responsible for the service.
  #      name: "My Org"
  #      # URL of the website of the organization (RFC 3986 format).
  #      url: "https://my.org"
  #
  #    # URL of the documentation of this service (RFC 3986 format). This should help someone to learn
  #    # how to use your service, including any specifics required to access data, e.g.
  #    # authentication.
  #    documentation_url:
  #      https://gitlab.com/one-touch-pipeline/weskit/documentation/
  #
  #  # Timestamp describing when the service was first deployed and available (RFC 3339 format).
  #  # Example: 2019-06-04T12:58:19Z
  #  created_at: 2021-06-04T12:58:19Z
  #
  #  # Timestamp describing when the service was last updated (RFC 3339 format).
  #  # TODO Set this during deployment?
  #  updated_at: 2021-06-04T12:58:19Z
  #
  #  # Version of the service being described. Semantic versioning is recommended, but other
  #  # identifiers, such as dates or commit hashes, are also allowed. The version should be changed
  #  # whenever the service is updated.
  #  version: 0.0.0
  #
  #  # Environment the service is running in. Use this to distinguish between production,
  #  # development and testing/staging deployments. Suggested values are prod, test, dev, staging.
  #  # However this is advised and not enforced.
  #  environment: *environment

  # URL of the contact for the provider of this service, e.g. a link to a contact form (RFC 3986
  # format), or an email (RFC 2368 format).
  contact_url:
    mailto:your@email.de

  supported_filesystem_protocols:
    - file
    - S3

  # A web page URL with human-readable instructions on how to get an authorization token for use
  # with a specific WES endpoint.
  auth_instructions_url: "https://somewhere.org"

  tags:
    tag1: value1
    tag2: value2


workflow_engines:

  # Note the "type" fields for the default parameters are optional and free text string. To express
  # the accepted value range for the parameter for the client, however, the existing examples use
  # the following syntax:
  #
  # * Use Python type annotation syntax.
  # * The default, if the "type" field is omitted here, is "str", which means unparsed.
  # * Strings to be accepted by specific processing are suggested to be written as
  #   `str validFor Python.fully.qualified.processing_function($)`
  #   where the `$` denotes the parameter that will be used to process the parameter. The `$`
  #   can be omitted if the parse function takes the string as first or only parameter.
  # * A regular expression matching exactly the allowed values.
  #
  # Valid parameter values:
  # * `Optional[str]`: null (JSON, YAML) or any string
  # * `Optional[str valid for Python.pathlib.Path]`: null or a string that is understood by Path
  #
  # The code that does the processing can be found in the `WorkflowEngine` subclasses. It is not
  # interpreted in any way.
  #
  # workflow engines are imported from the values.yaml
  # Documentation of workflow engine parameters can be found in the API repository:
  # https://gitlab.com/one-touch-pipeline/weskit/api in api/tests/weskit.yaml:workflwo_engines
  {{ toYaml .Values.weskit.workflow_engines | nindent 2 }}

cors:
  enabled: false

# login-configurationd are imported from the values.yaml
# Documentation of login-configuration parameters can be found at:
# https://gitlab.com/one-touch-pipeline/weskit/api in api/tests/weskit.yaml:login
login:
  {{ toYaml .Values.weskit.login | nindent 2 }}

# Celery configuration. Use keys and values according to
# https://docs.celeryproject.org/en/stable/userguide/configuration.html#configuration
#
# Note: CELERY_RESULT_BACKEND/RESULT_BACKEND, and BROKER_URL have *lower* priority.
#
# Note: The default configuration is in api/weskit/celeryconfig.py.
celery:
  # If True, the task will report their status as ‘started’ when the task is executed by a worker.
  task_track_started: true

  # Set the result expiration time to 7 days (default = 86400 sec = 1 d)
  result_expires: 604800
