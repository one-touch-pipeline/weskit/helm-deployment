# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

version: 1
disable_existing_loggers: False
formatters:
    standard:
        format: "%(asctime)s %(levelname)s %(message)s"
        datefmt: "%Y-%m-%dT%H:%M:%SZ"
handlers:
    console:
        class: logging.StreamHandler
        {{- if .Values.debugServices }}
        level: DEBUG
        {{- else }}
        level: INFO
        {{- end }}
        formatter: standard
        stream: ext://sys.stdout
root:
    handlers: [console]
    {{- if .Values.debugServices }}
    level: DEBUG
    {{- else }}
    level: INFO
    {{- end }}
asyncio:
    handlers: [console]
    # See https://docs.python.org/3/library/asyncio-dev.html#debug-mode
    {{- if .Values.debugServices }}
    level: DEBUG
    {{- else }}
    level: INFO
    {{- end }}
asyncssh:
    handlers: [console]
    {{- if .Values.debugServices }}
    level: DEBUG
    {{- else }}
    level: INFO
    {{- end }}
