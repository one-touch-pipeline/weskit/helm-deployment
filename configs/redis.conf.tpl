# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

# See https://raw.githubusercontent.com/redis/redis/6.0/redis.conf

# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably)
# warning (only very important / critical messages are logged)
{{- if .Values.debugServices }}
loglevel debug
{{- else }}
loglevel notice
{{- end }}

# Redis is used as queue, not as a cache. So never evict keys.
maxmemory-policy noeviction

# Limit the memory, only if you need to.
# maxmemory 1gb

# Persistence (https://redis.io/topics/persistence)

# Make a snapshot every minute, if at least a 1000 keys changed.
save 60 1000

# Additionally, use the append-only persistence.
appendonly yes
dir /data

# For every write sync to disc. Alternatives "everysec", "no" (= leave it to OS).
appendfsync always
