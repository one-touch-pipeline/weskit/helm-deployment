<!--
SPDX-FileCopyrightText: 2023 The WESkit Team

SPDX-License-Identifier: MIT
-->

# WESkit Kubernetes Deployment

The documentation can be found [here](https://gitlab.com/one-touch-pipeline/weskit/documentation/).

## Testing the Deployment

The directory `service-tests/` contains few simple scripts that probe the functionality of the deployment.

### Service Info

Access to the service information is not password protected, but may require a TLS certificate. The `service-tests/` script assumes that you have a Minikube deployment with TLS turned on. It assumes the TLS certificate is located in `certs/weskit.crt.pem` and uses it for validation.

### Test-Workflow

Few simple workflows for testing are delivered with the WESkit code (the workflow in `api/tests/wf1`). The `snakemake-test-without-login` and `snakemake-test-with-login` scripts run one of these workflows.

1. Copy the `config.yaml` from this directory and set the values.
2. To run the test that don't need login
   ```bash
   cd service-tests
   snakemake-test-without-login config.yaml
   ```
3. If you want to run the tests with login, you can do
   ```bash
   cd service-tests
   snakemake-test-with-login config.yaml
   ```

## SSH Keyfile

If you set `weskit.executor.type` to an executor that uses a nested SSH executor (to run the submission commands via SSH), then you need to also configure the `weskit.executor.login` section with `host` (the submission host), `username`, and `passphrase`. The the deployment currently only supports keyfile-based login, i.e. the `passphrase` should be that of the keyfile. Additionally, you have to put the keyfile as `ssh/cluster` and the and known hosts file as `ssh/knownhosts` into the `ssh/` directory.

E.g. you could do something like the following to generate the files:

```bash
yourUser=me
yourServer=submission-host
ssh-keyscan "$yourServer" > ssh/knownhosts
ssh-keygen -t ed25519 -f ssh/cluster
ssh-copy-id -i ssh/cluster "$yourUser@$yourServer"  
```

Note that the REST server reads the credentials only once, at server start.

## Development

We use [reuse](https://reuse.software) for the license management and annotation.
