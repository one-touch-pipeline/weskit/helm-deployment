# SPDX-FileCopyrightText: 2023 The WESkit Team
#
# SPDX-License-Identifier: MIT

{{/*
Expand the name of the chart.
*/}}
{{- define "weskit.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "weskit.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "weskit.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "weskit.labels" -}}
helm.sh/chart: {{ include "weskit.chart" . }}
{{ include "weskit.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "weskit.selectorLabels" -}}
app.kubernetes.io/name: {{ include "weskit.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "weskit.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "weskit.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Shortcut for the condition to use an SSH-based access to the cluster.
Note: The if/else and strange return values are because the template language strips off the types
      of values and only returns strings, and `false` becomes "false" which evaluates to `true`
      at call sites. See  https://github.com/helm/helm/issues/11231.
*/}}
{{- define "weskit.useSsh" -}}
{{- if (or (eq (.Values.weskit).executor.type "ssh") (eq (.Values.weskit).executor.type "ssh_lsf") (eq (.Values.weskit).executor.type "ssh_slurm")) }}
"true"
{{- else }}
{{- end }}
{{- end }}


{{/*
WESKIT_DATADIR in the WESkit service containers
*/}}
{{- define "weskit.dataDir" -}}
{{ default "/data" (.Values.weskit).dataDir }}
{{- end }}

{{/*
WESKIT_WORKFLOWS in the WESkit service containers
*/}}
{{- define "weskit.workflowsDir" -}}
{{ default "/workflows" (.Values.weskit).workflowsDir }}
{{- end }}

{{/*
WESKIT_DATADIR in the execution environment
*/}}
{{- define "weskit.executor.dataDir" -}}
{{ default (include "weskit.dataDir" .) (((.Values.weskit).executor).remote).dataDir }}
{{- end }}

{{/*
WESKIT_WORKFLOWS in the execution environment
*/}}
{{- define "weskit.executor.workflowsDir" -}}
{{ default (include "weskit.workflowsDir" .) (((.Values.weskit).executor).remote).workflowsDir }}
{{- end }}

{{/*
WESKIT_SINGULARITY_CONTAINERS in the execution environment
*/}}
{{- define "weskit.executor.singularityContainerDir" -}}
{{ default "/containers/singularity" (((.Values.weskit).executor).remote).singularityContainerDir }}
{{- end }}
